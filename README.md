# Neural Network Implementation #

Generating 4 sample datasets and experimenting with different implementations (python vs cuda) and different methods of training. Cuda is still a work in progress. Python visualizations are in /visualizations (example below), source in /python/nn.py.


![Picture](visualization/spiral_python_doubleLayer.png)

### Python ###

* Python 2.7 
* NumPy (I downloaded the entire Anaconda distribution)
* Plotly (https://plot.ly/python/getting-started/)
* Needs to be run from /python because of path reference to /visualization
* Model inputs are specified in in the inputs dict and the dimensions of the hidden layers (excluding inputs and binary output) are specified in layerDims
* Method call to run model is at the bottom

### CUDA ###

* VS 2013 Project
* Cuda 7.5
* Cublas
* Thrust