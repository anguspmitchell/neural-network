#include <iostream>
#include "utils.h"
#include <string>
#include <stdio.h>

void runModel();

int main(int argc, char **argv) {
  runModel();

  cudaDeviceSynchronize(); checkCudaErrors(cudaGetLastError());

  return 0;
}

