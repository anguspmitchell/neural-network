#include "utils.h"

const unsigned int MAX_CUDA_THREADS = 1024;

typedef struct _matrixDim
{
	unsigned int i, j;
	unsigned int getSize(){
		return i * j;
	}
} matrixDim;

typedef struct _datum
{
	float x1, x2, y;
} datum;

typedef struct _variableInputs
{
	bool x1, x2, x1x2, x1x1, x2x2, sinx1, sinx2;
	int getNumInputs()
	{
		int num = 0;
		if (x1) num++;
		if (x2) num++;
		if (x1x2) num++;
		if (x1x1) num++;
		if (x2x2) num++;
		if (sinx1) num++;
		if (sinx2) num++;
		return num;
	}
} variableInputs;

__global__ void printMatrixRowMajor(float* matrix, int i, int j)
{
	for (int index = 0; index < i * j; index++)
	{
		printf("%6f ", matrix[index]);
		if ((index + 1) % j == 0)
		{
			printf("\n");
		}
	}
	printf("\n");
}

__global__ void printList(float* list, int n)
{
	for (int i = 0; i < n; i++){
		printf("%f\n", list[i]);
	}
	printf("\n");
}

__global__ void printList(datum* list, int n)
{
	for (int i = 0; i < n; i++){
		printf("%f %f %f\n", list[i].x1, list[i].x2, list[i].y);
	}
	printf("\n");
}

void printDeviceVector(thrust::device_vector<datum> d_vector)
{
	printf("Size: %d\n", d_vector.size());
	datum *d_vector_ptr = thrust::raw_pointer_cast(d_vector.data());
	printList << <1, 1 >> >(d_vector_ptr, d_vector.size());
}

void printDeviceVector(thrust::device_vector<float> d_vector)
{
	printf("Size: %d\n", d_vector.size());
	float *d_vector_ptr = thrust::raw_pointer_cast(d_vector.data());
	printList << <1, 1 >> >(d_vector_ptr, d_vector.size());
}

void printDeviceMatrix(thrust::device_vector<float> d_vector, int i, int j)
{
	printf("Size: %d\n", d_vector.size());
	float *d_vector_ptr = thrust::raw_pointer_cast(d_vector.data());
	printMatrixRowMajor << <1, 1 >> >(d_vector_ptr, i, j);
}

__global__ void randomInit_uniform(float* matrix, unsigned int seed)
{
	curandState_t state;
	curand_init(seed, threadIdx.x + blockIdx.x * blockDim.x, 0, &state);

	matrix[threadIdx.x + blockIdx.x * blockDim.x] = curand_uniform(&state);
}

void vectorRandomInit_uniform(thrust::device_vector<float> &d_vector, int shift)
{
	float *d_vector_ptr = thrust::raw_pointer_cast(d_vector.data());
	int threads = std::min((int)MAX_CUDA_THREADS, (int)d_vector.size());
	int blocks = (d_vector.size() + threads - 1) / d_vector.size();
	randomInit_uniform << <blocks, threads >> >(d_vector_ptr, time(NULL) + shift);
}

__global__ void randomInit_twoGaussian(datum* list, unsigned int seed, 
	float mean_x1_0, float mean_x2_0, float sd_0,
	float mean_x1_1, float mean_x2_1, float sd_1)
{
	int idx = threadIdx.x + blockIdx.x * blockDim.x;
	curandState_t state;
	curand_init(seed, idx, 0, &state);
	
	float mean_x1, mean_x2, sd, y;
	if (idx % 2 == 0){
		mean_x1 = mean_x1_0;
		mean_x2 = mean_x2_0;
		sd = sd_0;
		y = 0.0;
	}
	else{
		mean_x1 = mean_x1_1;
		mean_x2 = mean_x2_1;
		sd = sd_1;
		y = 1.0;
	}

	list[idx].x1 = curand_normal(&state) *sd + mean_x1;
	list[idx].x2 = curand_normal(&state) *sd + mean_x2;
	list[idx].y = y;
}

void matrixMultiply(float *A, float *B, float *C, matrixDim dimA, matrixDim dimB, matrixDim dimC)
{
	if (dimA.i != dimC.i || dimB.j != dimC.j || dimA.j != dimB.i)
	{
		printf("(%u, %u) x (%u, %u) = (%u, %u)", dimA.i, dimA.j, dimB.i, dimB.j, dimC.i, dimC.j);
		return;
	}

	const float alpha = 1.0f;
	const float beta = 0.0f;
	cublasHandle_t handle;
	cublasCreate(&handle);
	cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, dimB.j, dimA.i, dimA.j, &alpha, B, dimB.j, A, dimA.j, &beta, C, dimB.j);
}

__device__ float calcCost(float h, float y){
	return y * -1 * logf(h) + (1 - y) * -1 * logf(1 - h);
}

__device__ void setInputMatrix(const datum iTrain, variableInputs inputs, float* const a){
	int counter = 0;
	if (inputs.x1) a[counter++] = iTrain.x1; 
	if (inputs.x2) a[counter++] = iTrain.x2;
	if (inputs.x1x2) a[counter++] = iTrain.x1 * iTrain.x2;
	if (inputs.x1x1) a[counter++] = powf(iTrain.x1, 2);
	if (inputs.x2x2) a[counter++] = powf(iTrain.x2, 2);
	if (inputs.sinx1) a[counter++] = sinf(iTrain.x1);
	if (inputs.sinx2) a[counter++] = sinf(iTrain.x2);
}

__global__ void propagate(float* const iCost, const datum* const train, variableInputs inputs,
	const float* const thetas, const float* const gradients, const int* const layerDims, 
	const int numInputs, const int totalNodes, int layerTransitions)
{
	int idx = threadIdx.x + blockDim.x * blockIdx.x;
	datum iTrain = train[idx];
	float y = iTrain.y;
	float cost = 0.f;
	float* a = new float[totalNodes];
	float* z = new float[totalNodes];
	float* d = new float[totalNodes];
	
	// Forward Prop
	setInputMatrix(iTrain, inputs, a);
	int aIndex = 0, zIndex = 0;
	int aSize = numInputs, zSize = layerDims[1];
	//printf("x1: %f x2: %f ", iTrain.x1, iTrain.x2);
	//for (int i = 0; i < numInputs; i++){
	//	printf("a[%d]: %f ", i, a[i]);
	//}
	//printf("\n");
	for (int kFwd = 0; kFwd < layerTransitions; kFwd++){
		kernelMultiply(z, zIndex, zSize)
	}

	// Calculate cost
	cost = calcCost(0.53, y);

	// Backward Prop
	for (int kBack = layerTransitions - 1; kBack >= 0; kBack--){

	}

	iCost[idx] = cost;
	free(a);
	free(z);
	free(d);
}

void trainNN(thrust::device_vector<datum> d_train, variableInputs inputs, 
	thrust::device_vector<int> layerDims, int iterations, float alpha, float lambda)
{
	// Dimensions
	int numInputs = inputs.getNumInputs();
	int numOutputs = 1;
	int layerTransitions = layerDims.size() + 1;
	int totalLayers = layerDims.size() + 2;
	int innerNodes = 0;
	for(int ld = 0; ld < layerDims.size(); ld++){
		innerNodes += layerDims[ld];
	}
	int totalNodes = numInputs + innerNodes + numOutputs;
	int m = d_train.size();
	//thrust::device_vector<float>* d_thetas = new thrust::device_vector<float>[layerTransitions];
	//thrust::device_vector<int> d_thetaDims_i[2]; // Couldn't get Thrust vector of structs to work
	//thrust::device_vector<int> d_thetaDims_j[2];
	
	// Initialize theta	
	thrust::device_vector<float> d_thetas; // Array of 2D Arrays --> 1 D  :(
	thrust::device_vector<matrixDim> d_thetaDims(layerTransitions);

	for (int k = 0; k < layerTransitions; k++){ // # Layers + 1 to cover input/output
		int mapFrom, mapTo;
		if (k == 0){
			mapFrom = numInputs;
		}
		else{
			mapFrom = layerDims[k - 1];
		}
		if (k == layerDims.size()){
			mapTo = numOutputs;
		}
		else{
			mapTo = layerDims[k];
		}
		thrust::device_vector<float> d_theta(mapTo * (mapFrom + 1)); // Add one for bias unit
		vectorRandomInit_uniform(d_theta, k);
		
		d_thetas.insert(d_thetas.end(), d_theta.begin(), d_theta.end());
		//printDeviceMatrix(d_theta, mapTo , (mapFrom + 1));
		matrixDim dim = { mapTo, mapFrom + 1 };
		//dim.i = mapTo;
		//dim.j = mapFrom + 1;
		d_thetaDims.push_back(dim);
	}
	//thrust::host_vector<int> h_thetaDims_i(2);
	//thrust::copy()
	//printDeviceMatrix(d_thetas[0], ((matrixDim)d_thetaDims[0]).i, ((matrixDim)d_thetaDims[0]).j);
	//printDeviceMatrix(d_thetas[1], ((matrixDim)d_thetaDims[1]).i, ((matrixDim)d_thetaDims[1]).j);
	printDeviceVector(d_thetas);


	for (int iter; iter < iterations; iter++){
		// Initialize Cost
		float J = 0;
		//int h_J, d_J;
		//h_J = 0;
		//checkCudaErrors(cudaMalloc(&d_J, sizeof(int)));
		//checkCudaErrors(cudaMemcpy(d_J, h_J, sizeof(int), cudaMemcpyHostToDevice));

		// Initialize Gradient
		//thrust::device_vector<float>* d_gradients = new thrust::device_vector<float>[layerTransitions];
		thrust::device_vector<float> d_gradients;
		for (int k = 0; k < d_thetaDims.size(); k++){
			thrust::device_vector<float> d_gradient(((matrixDim)d_thetaDims[k]).getSize());
			thrust::fill(d_gradient.begin(), d_gradient.end(), 0);
			d_gradients.insert(d_gradients.end(), d_gradient.begin(), d_gradient.end());
		}

		// Initialize Layers
		//thrust::device_vector<float>* d_a = new thrust::device_vector<float>[totalLayers];
		//thrust::device_vector<float>* d_z = new thrust::device_vector<float>[totalLayers];
		//thrust::device_vector<float>* d_d = new thrust::device_vector<float>[totalLayers];
		//thrust::device_vector<float> d_a (totalNodes);
		thrust::device_vector<float> d_z (totalNodes);
		thrust::device_vector<float> d_d (totalNodes);

		int threads = std::min((int)MAX_CUDA_THREADS, m);
		int blocks = (m + threads - 1) / m;
		thrust::device_vector<float> d_iCost(m);
		thrust::fill(d_iCost.begin(), d_iCost.end(), 0);

		float *d_iCost_ptr = thrust::raw_pointer_cast(d_iCost.data());
		datum *d_train_ptr = thrust::raw_pointer_cast(d_train.data());
		float *d_thetas_ptr = thrust::raw_pointer_cast(d_thetas.data());
		float *d_gradients_ptr = thrust::raw_pointer_cast(d_gradients.data());
		//float *d_a_ptr = thrust::raw_pointer_cast(d_a.data());
		propagate << <blocks, threads >> >(d_iCost_ptr, d_train_ptr, inputs,
			d_thetas_ptr, d_gradients_ptr, numInputs, totalNodes, layerTransitions);
		J = thrust::reduce(d_iCost.begin(), d_iCost.end(), (float)0.f, thrust::plus<float>());
		printf("J = %f\n", J);
		// Clean up mem
		//delete[] d_gradients;
		//delete[] d_a;
		//delete[] d_z;
		//delete[] d_d;
	}

	// Clean up mem
	//delete[] d_thetas;
}

void testNN()
{

}

void genTwoGaussian(thrust::device_vector<datum> &list, 
	float x1_0, float x2_0, float sd_0, float x1_1, float x2_1, float sd_1)
{
	datum *d_data_gauss_ptr = thrust::raw_pointer_cast(list.data());
	int threads = std::min((int)MAX_CUDA_THREADS, (int)list.size());
	int blocks = (list.size() + threads - 1) / list.size();
	randomInit_twoGaussian << <blocks, threads >> >
		(d_data_gauss_ptr, time(NULL), x1_0, x2_0, sd_0, x1_1, x2_1, sd_1);
}

void runModel()
{
	
	//float *d_A, *d_B, *d_C;
	//matrixDim dimA, dimB, dimC;
	//dimA.i = 3;
	//dimA.j = 2;
	//dimB.i = 2;
	//dimB.j = 3;
	//dimC.i = 3;
	//dimC.j = 3;
	//thrust::device_vector<float> d_A(dimA.i * dimA.j);
	//thrust::device_vector<float> d_B(dimB.i * dimB.j);
	//thrust::device_vector<float> d_C(dimC.i * dimC.j);
	////checkCudaErrors(cudaMalloc((void **)&d_A, sizeof(float) * dimA.i * dimA.j));
	////checkCudaErrors(cudaMalloc((void **)&d_B, sizeof(float) * dimB.i * dimB.j));
	////checkCudaErrors(cudaMalloc((void **)&d_C, sizeof(float) * dimC.i * dimC.j));

	//float *d_A_ptr = thrust::raw_pointer_cast(d_A.data());
	//float *d_B_ptr = thrust::raw_pointer_cast(d_B.data());
	//randomInit << <1, dimA.i * dimA.j >> >(d_A_ptr, time(NULL));
	//randomInit << <1, dimB.i * dimB.j >> >(d_B_ptr, time(NULL) + 1);
	//printMatrixRowMajor << <1, 1 >> >(d_A_ptr, dimA.i, dimA.j);
	//printMatrixRowMajor << <1, 1 >> >(d_B_ptr, dimB.i, dimB.j);

	//matrixMultiply(d_A, d_B, d_C, dimA, dimB, dimC);

	//printMatrixRowMajor << <1, 1 >> >(d_C, dimC.i, dimC.j);
	//cudaFree(d_A);
	//cudaFree(d_B);
	//cudaFree(d_C);

	// Model Settings
	int N = 100;
	int iterations = 100;
	float trainingSplit = 0.5;
	float validationSplit = 0.25;
	std::vector<float> alphas = { 1.0f }; // { 0.1f, 1.0f, 10.0f };
	std::vector<float> lambdas = { 0.1f }; // { 0.01f, 0.1f, 1.0f};
	thrust::host_vector<int> h_layerDims = { 2 };
	thrust::device_vector<int> d_layerDims = h_layerDims;
	variableInputs inputs;
	inputs.x1 = true;
	inputs.x2 = true;
	inputs.x1x2 = false;
	inputs.x1x1 = false;
	inputs.x2x2 = false;
	inputs.sinx1 = false;
	inputs.sinx2 = false;

	// Data
	thrust::device_vector<datum> d_data_gauss(N);
	genTwoGaussian(d_data_gauss, -2, -2, 1, 2, 2, 1);
	thrust::device_vector<datum> d_train_gauss(d_data_gauss.begin(), d_data_gauss.begin() + floor(N * trainingSplit));
	thrust::device_vector<datum> d_val_gauss(d_data_gauss.begin() + floor(N * trainingSplit), d_data_gauss.begin() + floor(N * (trainingSplit + validationSplit)));
	thrust::device_vector<datum> d_test_gauss(d_data_gauss.begin() + floor(N * (trainingSplit + validationSplit)), d_data_gauss.begin() + d_data_gauss.size());

	//printDeviceVector(d_data_gauss);
	//printDeviceVector(d_train_gauss);
	//printDeviceVector(d_val_gauss);
	//printDeviceVector(d_test_gauss);

	for (int a = 0; a < alphas.size(); a++){
		for (int l = 0; l < lambdas.size(); l++){
			printf("alpha %f lambda %f\n", alphas[a], lambdas[l]);
			trainNN(d_train_gauss, inputs, layerDims, iterations, alphas[a], lambdas[l]);
			testNN();
		}
	}
}
