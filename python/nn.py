import numpy as np
import math
import plotly
from plotly import tools
import plotly.plotly as py
import plotly.graph_objs as go

###############################
#         Helper Methods
###############################  

def genSpiral(n):
    splitN = int(n / 2)
    deltaT = 0
    X0 = np.empty([splitN, 1])
    Y0 = np.empty([splitN, 1])
    for i in range(splitN):
        r = float(i) / splitN * 5
        t = 1.75 * i / splitN * 2 * np.pi + deltaT
        X0[i] = r * np.sin(t) 
        Y0[i] = r * np.cos(t)
    cat0 = np.zeros(X0.shape)
    
    deltaT = np.pi
    r = list(range(splitN))
    r[:] = [float(x) / splitN * 5 for x in r]
    t = list(range(splitN))
    t[:] = [1.75 * float(x) / splitN * 2 * np.pi + deltaT for x in t]
    X1 = np.asarray(r) * np.sin(np.asarray(t))
    Y1 = np.asarray(r) * np.cos(np.asarray(t))
    cat1 = np.ones(Y1.shape)
    
    X = np.append(X0, X1)
    Y = np.append(Y0, Y1)
    cat = np.append(cat0, cat1)
    data = np.column_stack((X, Y, cat))
    np.random.shuffle(data)
    return data

def genTwoGaussian(x0, y0, sd0, x1, y1, sd1, n):
    X0 = np.random.normal(x0, sd0, n / 2)
    Y0 = np.random.normal(y0, sd0, n / 2)
    cat0 = np.zeros(X0.shape)
    
    X1 = np.random.normal(x1, sd1, n / 2)
    Y1 = np.random.normal(y1, sd1, n / 2)
    cat1 = np.ones(Y1.shape)
    
    X = np.append(X0, X1)
    Y = np.append(Y0, Y1)
    cat = np.append(cat0, cat1)
    data = np.column_stack((X, Y, cat))
    np.random.shuffle(data)
    return data

def genRingData(r, n):
    radii_inner = np.random.uniform(0, r * 0.5, n / 2)
    angles_inner = np.random.uniform(0, 2 * math.pi, n / 2)
    x_inner = radii_inner * np.sin(angles_inner)
    y_inner = radii_inner * np.cos(angles_inner)
    cat_inner = np.zeros(x_inner.shape)
    
    radii_outer = np.random.uniform(r * 0.7, r, n / 2)
    angles_outer = np.random.uniform(0, 2 * math.pi, n / 2)
    x_outer = radii_outer * np.sin(angles_outer)
    y_outer = radii_outer * np.cos(angles_outer)
    cat_outer = np.ones(y_outer.shape)
    
    X = np.append(x_inner, x_outer)
    Y = np.append(y_inner, y_outer)
    cat = np.append(cat_inner, cat_outer)
    data = np.column_stack((X, Y, cat))
    np.random.shuffle(data)
    return data

def xor(x, y):
    if((x > 0 and y < 0) or (x < 0 and y > 0)):
        return True
    return False

def pad(x, padding):
    return padding if x >= 0 else -1 * padding

def getXORData(size, n):
    padding = 2 * size * 0.03
    
    x = np.random.uniform(size * -1, size, n)
    y = np.random.uniform(size * -1, size, n)
    cat = np.zeros(x.shape)
    for i in range(x.shape[0]):
        x[i] += pad(x[i], padding)
        y[i] += pad(y[i], padding)
        if(xor(x[i], y[i])):
            cat[i] = 1
    data = np.column_stack((x, y, cat))
    np.random.shuffle(data)
    return data

# Not vectorized
def sigmoid(x):
    return 1 / (1 + np.exp(-1 * x))

# Not vectorized
def sigmoidGradient(x):
    return sigmoid(x) * (1 - sigmoid(x));
    
def matrixPrint(m):
    print ("   " + str(m).replace('\n','\n   '))

def calcCost(hx, y):
    # Difference between h(x) and y
    return y * -1 * np.log(hx) + (1 - y) * -1 * np.log(1 - hx)

def getInputMatrix(inputs, X):
    a1 = np.empty((0, 1))
    x1 = X[0]
    x2 = X[1]
    if(inputs['x1']):
        a1 = np.concatenate([a1, [[x1]]], axis = 0)
    if(inputs['x2']):
        a1 = np.concatenate([a1, [[x2]]], axis = 0)
    if(inputs['x1x2']):
        a1 = np.concatenate([a1, [[x1 * x2]]], axis = 0)
    if(inputs['x1x1']):
	a1 = np.concatenate([a1, [[x1 * x1]]], axis = 0)  
    if(inputs['x2x2']):
        a1 = np.concatenate([a1, [[x2 * x2]]], axis = 0)
    if(inputs['sin(x1)']):
        a1 = np.concatenate([a1, [[np.sin(x1)]]], axis = 0)
    if(inputs['sin(x2)']):
        a1 = np.concatenate([a1, [[np.sin(x2)]]], axis = 0)
    return a1
	
def getInputSize(inputs):
    size = 0
    if(inputs['x1']):
        size += 1
    if(inputs['x2']):
	size += 1  
    if(inputs['x1x2']):
        size += 1
    if(inputs['x1x1']):
	size += 1  
    if(inputs['x2x2']):
        size += 1
    if(inputs['sin(x1)']):
        size += 1
    if(inputs['sin(x2)']):
        size += 1
    return size

def getInputStrings(inputs):
    strings = []
    if(inputs['x1']):
        strings.append('x1')
    if(inputs['x2']):
	strings.append('x2')
    if(inputs['x1x2']):
        strings.append('x1*x2')
    if(inputs['x1x1']):
	strings.append('x1^2')
    if(inputs['x2x2']):
        strings.append('x2^2')
    if(inputs['sin(x1)']):
        strings.append('sin(x1)')
    if(inputs['sin(x2)']):
        strings.append('sin(x2)')
    return strings


###############################
#            Plot
###############################  

def plotCharts(train, test, models, optimalModel, 
        error, predictions, alphas, lambdas, layerDims, name):
    fig = tools.make_subplots(rows=2, cols=2, subplot_titles=(
                                        'Training Set', 
                                        'Training J(Theta)', 
                                        'Validation Error (Size) vs Lambda and Alpha', 
                                        'Test Results'))
    ### Training Set ###
    train_0 = train[train[:,2] == 0]
    train_1 = train[train[:,2] == 1]
    tr_train_0 = go.Scatter(
        x = train_0[:,0],
        y = train_0[:,1],
        mode = 'markers',
        name = 'Training Set 0',
        marker = dict(
            color = '#DDDDDD'
        )
    )
    
    tr_train_1 = go.Scatter(
        x = train_1[:,0],
        y = train_1[:,1],
        mode = 'markers',
        name = 'Training Set 1',
        marker = dict(
            color = '#000000'
        )
    )
    
    fig.append_trace(tr_train_0, 1, 1)
    fig.append_trace(tr_train_1, 1, 1)

    ### J(Theta) Training ###
    for nn in models:
        costs = nn['trainingCosts']
        tr_costs = go.Scatter(
            x = range(len(costs)),
            y = costs,
            mode = 'line',
            name = "alpha " + str(nn['alpha']) + " lambda " + str(nn['lambda'])
        )
        fig.append_trace(tr_costs, 1, 2)  
            
    fig['layout']['xaxis2'].update(title = 'Iterations')
    fig['layout']['yaxis2'].update(title = 'Cost', range = [0, 2]) 
   
    ### Validation Error ###
    errorColor = ['Red'] * len(models)
    for i, alpha in enumerate(alphas):
        for j, lambdaReg in enumerate(lambdas):
            if lambdaReg == optimalModel['lambda'] and alpha == optimalModel['alpha']:
                errorColor[j + i * len(lambdas)] = 'Green'
    
    tr_error = go.Scatter(
        x = [model['lambda'] for model in models],
        y = [model['alpha'] for model in models],
        mode = 'markers',
        name = 'Validation Error',
        marker = dict(
            size = [model['validationError'] + .05 for model in models],
            sizeref = 0.01,
            color = errorColor
        )
    )
    
    fig.append_trace(tr_error, 2, 1) 
    fig['layout']['xaxis3'].update(title = 'lambda', type = 'log')
    fig['layout']['yaxis3'].update(title = 'alpha', type = 'log')     
    
    ### Test Results ###
    test_0 = test[test[:,2] == 0]
    predictions_0 = predictions[np.ix_(test[:,2] == 0)]
    resultsPredicate_0 = np.equal(test_0[:,2], predictions_0)
    resultsColor_0 = np.ndarray(resultsPredicate_0.shape, dtype='a10')
    resultsColor_0[resultsPredicate_0 == True] = "#ADEBAD"
    resultsColor_0[resultsPredicate_0 == False] = "Red"
    resultsSymbol_0 = np.ndarray(resultsPredicate_0.shape, dtype='a10')
    resultsSymbol_0[resultsPredicate_0 == True] = "#FF9999"
    resultsSymbol_0[resultsPredicate_0 == False] = "x"
    
    tr_results_0 = go.Scatter(
        x = test_0[:,0],
        y = test_0[:,1],
        mode = 'markers',
        name = 'Test Set 0',
        marker = dict(
            color = resultsColor_0,
			symbol = resultsSymbol_0
        )
    )
        
    test_1 = test[test[:,2] == 1]  
    predictions_1 = predictions[np.ix_(test[:,2] == 1)]      
    resultsPredicate_1 = np.equal(test_1[:,2], predictions_1)
    resultsColor_1 = np.ndarray(resultsPredicate_1.shape, dtype='a10')
    resultsColor_1[resultsPredicate_1 == True] = "Green"
    resultsColor_1[resultsPredicate_1 == False] = "Red"
    resultsSymbol_1 = np.ndarray(resultsPredicate_1.shape, dtype='a10')
    resultsSymbol_1[resultsPredicate_1 == True] = "circle"
    resultsSymbol_1[resultsPredicate_1 == False] = "x"
    
    tr_results_1 = go.Scatter(
        x = test_1[:,0],
        y = test_1[:,1],
        mode = 'markers',
        name = 'Test Set 1',
        marker = dict(
            color = resultsColor_1,
			symbol = resultsSymbol_1
        )
    )

    fig.append_trace(tr_results_0, 2, 2)
    fig.append_trace(tr_results_1, 2, 2)
    fig['layout'].update(title = "Neural Network - " + name + " Dataset (Inputs: " + ", ".join(getInputStrings(inputs)) + "; Layers: " + ", ".join("{" + str(x) + "}" for x in layerDims) + ")")

    plotly.offline.plot(fig, filename = '../visualization/' + str(name) + '_python.html')
    
###############################
#          Train NN
###############################  

def trainNN(train, inputs, layerDims, iterations, alpha, lambdaReg, debug):
    costs = []
    
    # Initialize theta
    numInputs = getInputSize(inputs)
    numOutputs = 1
    thetas = []
    for k in range(len(layerDims) + 1): # One extra to cover input/output
        if k == 0:
            mapFrom = numInputs
        else:
            mapFrom = layerDims[k - 1]
        if k == len(layerDims):
            mapTo = numOutputs
        else:
            mapTo = layerDims[k]
        theta = np.random.rand(mapTo, mapFrom + 1) # Add one for bias unit
        thetas.append(theta)

    for iter in range(iterations):
        # Initialize cost
        J = 0
        
        # Initialize gradient
        gradients = []
        for k in range(len(thetas)):
            gradient = np.zeros(thetas[k].shape)
            gradients.append(gradient)

        # Initialize Layers
        a = [None] * (len(thetas) + 1) # Add 1 for end layer
        z = [None] * (len(thetas) + 1) # z[0] will be None
        d = [None] * (len(thetas) + 1) # d[0] will be None
        
        
        m = train.shape[0]
        for i in range(m):
            y = train[i, 2]
            
            ### Forward Propagation ###
            for kFwd in range(len(layerDims) + 1):
                theta = thetas[kFwd]
                if(kFwd == 0):
                    a_prev = getInputMatrix(inputs, train[i, :-1])
                    a[kFwd] = a_prev
                else:
                    a_prev = a[kFwd]
                z_next = theta.dot(np.concatenate([[[1]], a_prev], axis = 0))
                z[kFwd + 1] = z_next
                a_next = np.empty(z_next.shape)
                for s in range(z_next.shape[0]):
                    a_next[s] = sigmoid(z_next[s])
                a[kFwd + 1] = a_next

                if debug['any'] and debug['fwd']:
                    print "\nFORWARD " + str(kFwd) + " --> " + str(kFwd + 1)
                    print "a" + str(kFwd)
                    matrixPrint(a_prev)
                    print "theta" + str(kFwd) + " * [1; a" + str(kFwd) + "] ="
                    matrixPrint(theta)
                    matrixPrint("*")
                    matrixPrint(np.concatenate([[[1]], a_prev], axis = 0))
                    print "= z" + str(kFwd + 1)
                    matrixPrint(z_next)
                    if kFwd == len(layerDims):
                        print "sigmoid(z) = a" + str(kFwd + 1) + " = h(x)"
                    else:
                        print "sigmoid(z) = a" + str(kFwd + 1)
                    matrixPrint(a_next)

            Ji = calcCost(a[-1][0], y)
            J += Ji
        
            if debug['any'] and debug['fwd']:
                print "\nSTOP"
                print "cost(i) of cost"
                matrixPrint(str(Ji) + " of " + str(J))
            
            ### Backward Propagation ###
            for kBack in reversed(range(len(layerDims) + 1)):
                # for kBack = 2, d_prev would be layer 2 and d_next would be layer 1     
                if(kBack == len(layerDims)):
                    d_prev = np.concatenate([[[1]], a[-1] - y], axis = 0)
                    d[kBack + 1] = d_prev
                else:
                    d_prev = d[kBack + 1]
                
                gradient_i = d_prev[1:].dot(np.concatenate([[[1]], a[kBack]]).transpose())
                gradients[kBack] += gradient_i
                if kBack > 0:
                    sigmoidGradient_next = np.empty(z[kBack].shape)
                    for s in range(sigmoidGradient_next.shape[0]):
                        sigmoidGradient_next[s] = sigmoidGradient(z[kBack][s])

                    d_next = (thetas[kBack].transpose()).dot(d_prev[1:]) * np.concatenate([[[1]], sigmoidGradient_next], axis = 0)
                    d[kBack] = d_next

                if debug['any'] and debug['back']:
                    print "\nBACKWARD " + str(kBack + 1) + " ---> " + str(kBack)
                    print "d" + str(kBack + 1)
                    matrixPrint(d_prev)
                    print "d" + str(kBack + 1) + "[1:] * [1; a" + str(kBack) + "].T"
                    matrixPrint(d_prev[1:])
                    matrixPrint("*")
                    matrixPrint(np.concatenate([[[1]], a[kBack]]).transpose())
                    print "= gradient_theta" + str(kBack) + "(" + str(i) + ")"
                    matrixPrint(gradient_i)
            
            if debug['any'] and (debug['fwd'] or debug['back']):
                print "----------------"
        ### END i = 0 to m loop ###
    
        # Cost
        J = J / m
        J_reg = 0
        for k in range(len(thetas)):
            J_reg += ((thetas[k][:, 1:] * thetas[k][:, 1:]).sum())
        J_reg = J_reg * lambdaReg / 2 / m
    
        if(debug['any'] and debug['cost']):
            print "\nCOST"
            print "Nonregularized: " + str(J)
            print "Regularized: " + str(J + J_reg)
        J += J_reg  

        # Gradient
        for k in range(len(gradients)):
            gradients[k] = gradients[k] / m
            gradients[k][:, 1:] += (thetas[k][:, 1:] * lambdaReg / m)
        
        for k in range(len(thetas)):
            thetas[k] -= alpha * gradients[k]
    
        if(debug['any'] and debug['grad']):
            print "\nGRADIENT"
            for k in range(len(gradients)):
                print "gradient_theta" + str(k)
                matrixPrint(gradients[k])
            
        if(debug['any'] and 
            (debug['grad'] or debug['fwd'] or debug['back'] or debug['cost'])):
            print "-------------------------------"
            print "-------------------------------"
        costs.append(J.item(0))
    ### END Grad Descent Iteration Loop
    
    return thetas, costs

###############################
#           Test NN
###############################  

def testNN(test, inputs, thetas):
    results = np.empty(test[:,2].shape)
    
    # Initialize Layers
    a = [None] * (len(thetas) + 1) # Add 1 for end layer
    z = [None] * (len(thetas) + 1) # z[0] will be None
    d = [None] * (len(thetas) + 1) # d[0] will be None
    m = test.shape[0]
    for i in range(test.shape[0]):
        y = test[i, 2]

        for k in range(len(thetas)):
            theta = thetas[k]
            if k == 0:
                a_prev = getInputMatrix(inputs, test[i, :-1])
                a[k] = a_prev
            else:
                a_prev = a[k]
            z_next = theta.dot(np.concatenate([[[1]], a_prev], axis = 0))
            a_next = np.empty(z_next.shape)
            for s in range(z_next.shape[0]):
                a_next[s] = sigmoid(z_next[s])
            a[k + 1] = a_next
            if k == len(thetas) - 1:
                results[i] = 1.0 if a_next[0].item() > 0.5 else 0.0
    error = np.abs(test[:,-1] - results).sum() / m
    return error, results

###############################
#       Driver Method
###############################  

def run(train, val, test, layerDims, inputs, alphas, lambdas, iterations, name):
    models = []
    for alpha in alphas:
        for lambdaReg in lambdas:
            if debug['any'] and debug['overview']:
                print "Gaussian: Training alpha = " + str(alpha) + " lambda = " + str(lambdaReg)
        
            thetas, costs = trainNN(train, inputs, layerDims, iterations, alpha, lambdaReg, debug)
    
            if debug['any'] and debug['overview']:
                print "Gaussian: Cross Validating alpha = " + str(alpha) + " lambda = " + str(lambdaReg)
    
            valError, valPredictions = testNN(val, inputs, thetas)
            models.append({'thetas': thetas, 'alpha':alpha, 'lambda':lambdaReg, 'validationError': valError, 'trainingCosts': costs})
    model = sorted(models, key = lambda x: x['validationError'])[0]
    if debug['any'] and debug['overview']:
        print "Gaussian: Testing alpha = " + str(model['alpha']) + " lambda = " + str(model['lambda'])
    error, predictions = testNN(test, inputs, model['thetas'])
    
    if plot:
        plotCharts(train, test, models, model, 
            error, predictions, alphas, lambdas, layerDims, name)

###############################
#          Run Model
###############################   

N = 600
iterations = 2000
trainingSplit = 0.5
validationSplit = 0.25
plot = True
debug = {'any': True, 'overview': True, 'fwd': False, 'back': False, 'cost': False, 'grad': False}
alphas = [0.1, 1, 10]
lambdas = [0.01, 0.1, 1]
layerDims = [7, 7]
inputs = {'x1': True, 'x2': True, 'x1x2': True, 'x1x1': True, 'x2x2': True, 'sin(x1)': True, 'sin(x2)': True}

### Data Gen ###
if debug['any'] and debug['overview']:
    print "Generating data" 
data_gauss = genTwoGaussian(-2, -2, 1, 2, 2, 1, N)
data_ring = genRingData(1.0, N)
data_xor = getXORData(1, N)
data_spiral = genSpiral(N)

train_gauss = data_gauss[:int(N*trainingSplit)]
train_ring = data_ring[:int(N*trainingSplit)]
train_xor = data_xor[:int(N*trainingSplit)]
train_spiral = data_spiral[:int(N*trainingSplit)]
val_gauss = data_gauss[int(N*trainingSplit):int(N*(trainingSplit + validationSplit))]
val_ring = data_ring[int(N*trainingSplit):int(N*(trainingSplit + validationSplit))]
val_xor = data_xor[int(N*trainingSplit):int(N*(trainingSplit + validationSplit))]
val_spiral = data_spiral[int(N*trainingSplit):int(N*(trainingSplit + validationSplit))]
test_gauss = data_gauss[int(N*(trainingSplit + validationSplit)):]
test_ring = data_ring[int(N*(trainingSplit + validationSplit)):]
test_xor = data_xor[int(N*(trainingSplit + validationSplit)):]
test_spiral = data_spiral[int(N*(trainingSplit + validationSplit)):]

### Run Model ###
#run(train_gauss, val_gauss, test_gauss, layerDims, inputs, 
#    alphas, lambdas, iterations, 'Gaussian')
#run(train_ring, val_ring, test_ring, layerDims, inputs, 
#    alphas, lambdas, iterations, 'Ring')
#run(train_xor, val_xor, test_xor, layerDims, inputs, 
#    alphas, lambdas, iterations, 'XOR')
run(train_spiral, val_spiral, test_spiral, layerDims, inputs,
    alphas, lambdas, iterations, 'Spiral')
